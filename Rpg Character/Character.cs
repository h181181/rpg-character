﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Rpg_Character
{
    abstract class Character
    {
        #region attributes
        private string type;
        private string name;
        private int hp;
        private int energy;
        private int armourRating;

        public string Type { get => type; set => type = value; }
        public string Name { get => name; set => name = value; }
        public int Hp { get => hp; set => hp = value; }
        public int Energy { get => energy; set => energy = value; }
        public int ArmourRating { get => armourRating; set => armourRating = value; }
        #endregion

        #region constructor
        public Character(string name)
        {
        }
        public Character(string type, string name, int hp, int energy, int armourRating)
        {
            this.Type = type;
            this.Name = name;
            this.Hp = hp;
            this.Energy = energy;
            this.ArmourRating = armourRating;
        }
        #endregion

        #region methods
        //methods move and attack that can be overwritten
        public virtual void Move()
        {
            Console.WriteLine($"{Name} just moved\n");
        }
        public virtual void Attack()
        {
            Console.WriteLine($"{Name} just attacked!");
        }

        //prints out character information
        public void PrintInfo() {
            Console.WriteLine($"Type: {Type}\n" +
                $"Name: {Name} \n" +
                $"Health points: {Hp} \n" +
                $"Energy points: {Energy} \n" +
                $"Armour rating: {ArmourRating}");
        }
        #endregion
    }
}
