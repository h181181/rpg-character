﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Rpg_Character
{
    class DothrakiWarrior: Warrior
    {
        private string type;
        private string name; 
        public DothrakiWarrior(string type, string name, int hp, int energy, int armourRating) : base(type, name, hp, energy, armourRating)
        {
            this.Name1 = name;
        }

        public string Type1 { get => type; set => type = value; }
        public string Name1 { get => name; set => name = value; }

        public override void Attack() {
            Console.WriteLine($"{Type1} {Name1}just made a kill"); 
        }
        
    }
}
