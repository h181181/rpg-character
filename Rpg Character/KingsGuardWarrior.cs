﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Rpg_Character
{
    class Kingsguard: Warrior
    {
        private string type;
        private string name; 
        public Kingsguard(string type, string name, int hp, int energy, int armourRating) : base(type, name, hp, energy, armourRating)
        {
            this.Name1 = name;
        }

        public string Type1 { get => type; set => type = value; }
        public string Name1 { get => name; set => name = value; }

        public override void Attack()
        {
            Console.WriteLine($"{Name1} just cut off the head off"); 
        }
    }
}
