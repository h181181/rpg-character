﻿using System;
using System.Collections.Generic;

namespace Rpg_Character
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("These are your characters: \n");

            Wizard wiz = new Wizard("Wizard", "Wiz Khalifa", 200, 20, 1 );
            Kingsguard jamieLannister = new Kingsguard("Warrior", "Jamie Lannister", 400, 40, 10);
            DothrakiWarrior khalDrogo = new DothrakiWarrior("Dothraki Warrior", "Khal Drogo", 500, 50, 8);
            Thief alladin = new Thief("Thief", "Alladin", 100, 10, 2);

            //create new list of characters
            List<Character> newCharacter = new List<Character>();
            //adds character to list
            newCharacter.Add(wiz);
            newCharacter.Add(jamieLannister);
            newCharacter.Add(khalDrogo);
            newCharacter.Add(alladin);

            for (int i = 0; i < newCharacter.Count; i++)
            {
                Console.WriteLine(newCharacter[i].Name);
            }

            Console.WriteLine();

            //Prints out all of the info about the characters
            Console.WriteLine("Here's more information about them: \n");
            for (int i = 0; i < newCharacter.Count; i++)
            {
                newCharacter[i].PrintInfo();
                Console.WriteLine();

            }        

        }
    }
}
