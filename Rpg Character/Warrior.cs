﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Rpg_Character
{
    abstract class Warrior : Character
    {
        public Warrior(string type, string name, int hp, int energy, int armourRating) : base(type, name, hp, energy, armourRating)
        {

        }
    }
}
